package com.codekazes.controllers;

import com.codekazes.models.User;
import com.codekazes.services.LoginService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController {

    @Autowired
    LoginService loginService;


    @GetMapping("/login")
    public ModelAndView getLogin(){
        ModelAndView login = new ModelAndView("login");
        return login;
    }

    @PostMapping("/login")
    public String login(@RequestParam("email")String email,
                        @RequestParam("password")String password,

                        HttpSession session, RedirectAttributes redirect){

        if (email.isEmpty() || password.isEmpty() ){
            redirect.addFlashAttribute("error", "Email or Password can´t be empty.");
            return "redirect:/login";
        }

        String getEmailInDataBase = loginService.checkEmail(email);
        String getPasswordInDataBase = loginService.checkPassword(email);

        if(getEmailInDataBase == null){
            redirect.addFlashAttribute("error", "Email not registered yet.");
            return "redirect:/login";
        }

        if (!BCrypt.checkpw(password, getPasswordInDataBase)){
            redirect.addFlashAttribute("error", "Email or password not correct.");
            return "redirect:/login";
        }

        //LOGIN AT USER
        User user = loginService.getUserDetails(email);

        //SET SESSION ATTRIBUTE

        session.setAttribute("user", user);
        session.setAttribute("authenticated", true);

        return "redirect:/question1";
    }

    @GetMapping("/logout")
    public String logout(HttpSession session, RedirectAttributes redirect){
        session.invalidate();
        redirect.addFlashAttribute("logout", "Successful logout!");

        return "redirect:/login";
    }
}







