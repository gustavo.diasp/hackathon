package com.codekazes.controllers;

import com.codekazes.models.User;
import com.codekazes.services.RegistrationService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Random;


@Controller
public class RegisterController {

    @Autowired
   private RegistrationService registrationService;



    @GetMapping("/register")
    public ModelAndView getRegister(User user){
        ModelAndView register = new ModelAndView("register");

        return register;
    }


    @PostMapping("/register")
    public String createUser(@Valid  User user, BindingResult result, RedirectAttributes redirect,
                             @RequestParam("password") String password,
                             @RequestParam("email")String email,
                             @RequestParam("confirm_password") String confirmPassword){

        if (result.hasErrors()) {
            String errors = "";

            for(int i = 0; i < result.getAllErrors().size(); i++){
                errors += result.getAllErrors().get(i).getDefaultMessage() + "<br> ";
            }
            redirect.addFlashAttribute("error", errors);
            return "redirect:/register";
        }

        if (!password.equals(confirmPassword)){
            redirect.addFlashAttribute("error", "Password and Confirm Password don't match.");

            return "redirect:/register";
        }

        String getEmailInDataBase = registrationService.checkEmail(email);
        if (getEmailInDataBase != null){
            redirect.addFlashAttribute("error", "Email already registered.");

            return "redirect:/register";
        }

        //ENCRYPT PASSWORD
        user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));

        //CREATE AND INCLUDE USER AT DB
        registrationService.save(user);

        redirect.addFlashAttribute("message", "User criado");
        return "redirect:/register";
    }


}
