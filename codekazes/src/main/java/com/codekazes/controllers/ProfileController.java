package com.codekazes.controllers;

import com.codekazes.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProfileController {

    @Autowired
    ProfileService profileService;


    @GetMapping("/question1")
    public String getQuestion1() {
        return "question1";
    }

    @GetMapping("/question2")
    public String getQuestion2() {
        return "question2";
    }

    @GetMapping("/question3")
    public String getQuestion3() {
        return "question3";
    }

    @GetMapping("/question4")
    public String getQuestion4() {
        return "question4";
    }

    @GetMapping("/question5")
    public String getQuestion5() {
        return "question5";
    }

    @GetMapping("/cards")
    public String getCards() {
        return "cards";
    }

}

