package com.codekazes.services;

import com.codekazes.models.User;
import com.codekazes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {


    @Autowired
    UserRepository userRepository;

    public void save(User user){
        this.userRepository.save(user);
    }

    public String checkEmail(String email){
        return this.userRepository.checkEmail(email);
    }
}
