package com.codekazes.services;


import com.codekazes.models.Profile;
import com.codekazes.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileService {

    @Autowired
    ProfileRepository profileRepository;

    public List<Profile> getAccountByUser(Long user_id){
        return this.profileRepository.getAccountByUser(user_id);
    }
}
