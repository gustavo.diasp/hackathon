package com.codekazes.services;


import com.codekazes.models.User;
import com.codekazes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    @Autowired
    UserRepository userRepository;

    public String checkEmail(String email){
        return this.userRepository.checkEmail(email);
    }

    public String checkPassword(String email){
        return this.userRepository.checkPassword(email);
    }

    public User getUserDetails(String email){
        return this.userRepository.getUserDetails(email);
    }


}
