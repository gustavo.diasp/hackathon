package com.codekazes.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.Data;

@Entity(name = "user")
@Table(name = "users")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long user_id;

    @NotEmpty(message = "The First Name cannot be empty.")
    @Size(min = 3, max = 30, message = "Size must be between 3 and 30.")
    private String first_name;

    @NotEmpty(message = "The Last Name cannot be empty.")
    @Size(min = 3, max = 30, message = "Size must be between 3 and 30.")
    private String last_name;

    @Email
    @NotEmpty(message = "The email cannot be empty.")
    @Pattern(regexp = "([a-zA-Z0-9]+(?:[._+-][a-zA-Z0-9]+)*)@([a-zA-Z0-9]+(?:[.-][a-zA-Z0-9]+)*[.][a-zA-Z]{2,})", message = "Please insert a valid e-mail")
    private String email;

    @NotEmpty(message = "The password cannot be empty.")
    @NotNull
    private String password;

    @NotEmpty(message = "The confirm password cannot be empty.")
    @Transient
    private String confirm_password;
}
