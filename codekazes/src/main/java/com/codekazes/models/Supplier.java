package com.codekazes.models;

import com.codekazes.services.ProfileService;
import jakarta.persistence.*;
import lombok.Data;

@Entity(name = "supplier")
@Table(name = "suppliers")
@Data
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long user_id;
    private int ansiedade;
    private int calma;
    private int tesao;
    private int nostalgia;
    private int surpresa;
    @ManyToOne
    @JoinColumn (name = "profile_id")
    private Profile profile;

}
