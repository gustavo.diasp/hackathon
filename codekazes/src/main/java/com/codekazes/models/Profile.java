package com.codekazes.models;

import jakarta.persistence.*;
import lombok.Data;

@Entity(name = "profile")
@Table(name = "profiles")
@Data
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long profile_id;
    private int ansiedade;
    private int calma;
    private int tesao;
    private int nostalgia;
    private int surpresa;
    @ManyToOne
    @JoinColumn (name = "user_id")
    private User user;
}
