package com.codekazes.repository;


import com.codekazes.models.Profile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ProfileRepository extends CrudRepository<Profile, Long> {

    @Query(value = "SELECT * FROM profiles WHERE user_id = :user_id", nativeQuery = true)
    List<Profile> getAccountByUser(@Param("user_id")Long user_id);
}
