package com.codekazes.repository;

import com.codekazes.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    @Query(value = "SELECT email FROM users WHERE email = :email", nativeQuery = true)
    String checkEmail(@Param("email")String email);

    @Query(value = "SELECT password FROM users WHERE email = :email", nativeQuery = true)
    String checkPassword(@Param("email")String email);

    @Query(value = "SELECT * FROM users WHERE email = :email", nativeQuery = true)
    User getUserDetails(@Param("email")String email);

}
