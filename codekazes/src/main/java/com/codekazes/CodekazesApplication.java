package com.codekazes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodekazesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodekazesApplication.class, args);
	}

}
